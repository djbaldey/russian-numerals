# -*- coding: utf-8 -*-
#
# Copyright (c) 2017, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from __future__ import unicode_literals

import unittest
from decimal import Decimal
from russian_numerals import prepare


class TestPrepare(unittest.TestCase):

    def test_number_to_words(self):
        self.assertEqual(prepare('1'), 'один')
        self.assertEqual(prepare(1), 'один')
        self.assertEqual(prepare(1.0), 'один')
        self.assertEqual(prepare(Decimal('1.0')), 'один')

    def test_number_to_roubles(self):
        self.assertEqual(prepare('1', 'roubles'), 'один рубль')

    def test_number_to_tons(self):
        self.assertEqual(prepare('1', 'tons'), 'одна тонна')

    def test_number_to_kilograms(self):
        self.assertEqual(prepare('1', 'kilograms'), 'один килограмм')

    def test_text_to_numbers(self):
        self.assertEqual(prepare('один килограмм'), '1 килограмм')
        self.assertEqual(prepare('два килограмма'), '2 килограмма')
