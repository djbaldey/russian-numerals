# -*- coding: utf-8 -*-
#
# Copyright (c) 2017, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from __future__ import unicode_literals

import unittest
# from decimal import Decimal
from russian_numerals import handlers


class TestNumberToWords(unittest.TestCase):

    def test_string_number(self):
        func = handlers.NumberToWords().prepare
        self.assertEqual(func('1'), 'один')
        self.assertEqual(func('2'), 'два')
        self.assertEqual(func('3'), 'три')
        self.assertEqual(func('4'), 'четыре')
        self.assertEqual(func('5'), 'пять')
        self.assertEqual(func('6'), 'шесть')


class TestNumberToRoubles(unittest.TestCase):

    def test_string_number(self):
        func = handlers.NumberToRoubles().prepare
        self.assertEqual(func('1'), 'один рубль')
        self.assertEqual(func('2'), 'два рубля')
        self.assertEqual(func('3'), 'три рубля')
        self.assertEqual(func('4'), 'четыре рубля')
        self.assertEqual(func('5'), 'пять рублей')
        self.assertEqual(func('6'), 'шесть рублей')


class TestNumberToTons(unittest.TestCase):

    def test_string_number(self):
        func = handlers.NumberToTons().prepare
        self.assertEqual(func('1'), 'одна тонна')
        self.assertEqual(func('2'), 'две тонны')
        self.assertEqual(func('3'), 'три тонны')
        self.assertEqual(func('4'), 'четыре тонны')
        self.assertEqual(func('5'), 'пять тонн')
        self.assertEqual(func('6'), 'шесть тонн')


class TestNumberToKilograms(unittest.TestCase):

    def test_string_number(self):
        func = handlers.NumberToKilograms().prepare
        self.assertEqual(func('1'), 'один килограмм')
        self.assertEqual(func('2'), 'два килограмма')
        self.assertEqual(func('3'), 'три килограмма')
        self.assertEqual(func('4'), 'четыре килограмма')
        self.assertEqual(func('5'), 'пять килограммов')
        self.assertEqual(func('6'), 'шесть килограммов')


class TestTextToNumbers(unittest.TestCase):

    def test_to_numbers(self):
        func = handlers.TextToNumbers().prepare
        self.assertEqual(func('один килограмм'), '1 килограмм')
        self.assertEqual(
            func('плюс семь девятьсот восемьдесят семь шестьсот '
                 'пятьдесят четыре тридцать два десять'),
            'плюс 7 987 654 32 10'
        )


class TestTextToPhone(unittest.TestCase):

    def test_phone(self):
        func = handlers.TextToPhone().prepare
        self.assertEqual(
            func('плюс семь девятьсот восемьдесят семь шестьсот '
                 'пятьдесят четыре тридцать два десять'),
            '+79876543210'
        )
        self.assertEqual(
            func('плюс семь девятьсот восемьдесят семь шестьсот '
                 'пятьдесят четыре тридцать два десять мусорный текст'),
            '+79876543210'
        )

    def test_any_text(self):
        func = handlers.TextToPhone().prepare
        self.assertEqual(func('один килограмм'), '1')
        self.assertEqual(func('плюс один килограмм'), '+1')
        self.assertEqual(func('один плюс два килограмма'), '1')
