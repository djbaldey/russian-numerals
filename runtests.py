#!/usr/bin/env python
import sys
from unittest import TestLoader, TextTestRunner


if __name__ == '__main__':
    suite = TestLoader().discover('tests')
    result = TextTestRunner(verbosity=2).run(suite)
    sys.exit(bool(result.failures))
